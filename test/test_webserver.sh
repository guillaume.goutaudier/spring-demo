#!/bin/bash

# Set to webserver endpoint
ENDPOINT=https://webserver.octank.org


# Create tutorial with random title and content 
create_tutorial () {
  sleep 1
  LOREM=`lorem --randomize`
  TITLE="tutorial_$RANDOM"
  echo
  echo
  echo "Adding new tutorial: $TITLE, $LOREM"
  DATA="{\"title\": \"$TITLE\", \"description\": \"$LOREM\"}"
  echo "Request content: $DATA"
  curl -X POST -H 'Content-Type: application/json' -d "$DATA" $ENDPOINT/api/tutorials
}

# Get random tutorial
get_tutorial () {
  LIST=(91 92 93 94 95 96 97 98 99 100)
  RAND=$(($RANDOM % 9 + 1))
  TUTORIAL_ID=${LIST[$RAND]} 
  echo
  echo
  echo "Getting tutorial $TUTORIAL_ID"
  curl -X GET "$ENDPOINT/api/tutorials/$TUTORIAL_ID"
}


# Main
while true; do
  create_tutorial;
  sleep 1;
  get_tutorial;
  sleep 1;
done

