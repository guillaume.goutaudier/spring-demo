# spring-demo

The objective of this tutorial is to demonstrate how to migrate and modernize a Java 11 spring application running on Oracle 11g to AWS.
The application is based on the instructions from this tutorial:
https://www.bezkoder.com/spring-boot-jdbctemplate-example-oracle/

The application will be migrated and modernized using this architecture:
![Octank_Migration.png](architecture/Octank_Migration.png)

# Web server installation
Deploy an Ubuntu 20.04 web server on the public subnet. 
We will use it as a jumpbox to connect to the Database server.

# Database server install
- Deploy Windows server 2012 R2 
- Use SSH port forwarding to connect to the server:
```
export WEBSERVER_PUBLIC_IP=15.236.35.181
export DBSERVER_PRIVATE_IP=10.0.1.59
ssh -o ServerAliveInterval=10 -L 3389:$DBSERVER_PRIVATE_IP:3389 ubuntu@$WEBSERVER_PUBLIC_IP
```

- Enable downloads:
https://answers.microsoft.com/en-us/ie/forum/all/error-message-your-current-security-settings-do/59cc236d-7baf-4552-92ff-b34b9a6942aa
- Install Oracle database following these instruction:
https://www.geeksforgeeks.org/how-to-install-oracle-database-11g-on-windows/
- Create an "octank" database with the following parameters:
```
"octank" DB password (SYS, SYSTEM) = ainu9Jui
```
- Disable windows firewall
- Allow DB remote access:
Launch Net Manager >> Replace localhost by 0.0.0.0 in the local listener configuration


# Web server configuration

### sqlplus install
- Follow the instructions from this link:
https://gist.github.com/bmaupin/1d376476a2b6548889b4dd95663ede58

- After installation, validate that the DB can be accessed:
```
export DBSERVER_PRIVATE_IP=10.0.1.59
sqlplus64 sys/ainu9Jui@//$DBSERVER_PRIVATE_IP/octank as sysdba
```

### Oracle DB configuration
- Create dbadmin user:
```
SQL> @create_dbadmin_user.sql
```

- Reconnect with dbadmin user and create the tutotials table:
```
export DBSERVER_PRIVATE_IP=10.0.1.59
sqlplus64 dbadmin/515e6deb49cb40c0@//$DBSERVER_PRIVATE_IP:1521/octank
SQL> @create_tutotials_table.sql
SQL> @create_trigger.sql
```

### Oracle JDK 11 install:
- Follow the instructions from this link:
https://www.digitalocean.com/community/tutorials/how-to-install-java-with-apt-on-ubuntu-20-04

NB: at the time we did the install, the right version to use was jdk-11.0.13_linux-x64_bin.tar.gz



### Spring Configuration
- Update `src/main/resources/application.properties` with the right DB URL:
```
spring.datasource.url=jdbc:oracle:thin:@<db_server_ip>:1521/octank
```

### JAR creation 
- Create a JAR file and create a system service following this tutorial:
https://codergists.com/camel/java/linux/2019/05/01/run-spring-boot-jar-as-a-linux-service.html


### Test Application
- Start the application:
```
/usr/bin/java -jar /home/ubuntu/spring_demo/target/demo-0.0.1-SNAPSHOT.jar 
# Alternative: ./mvnw spring-boot:run
```
- Test the application:
```
export ONPREMISE_WEBSERVER_ENDPOINT=http://15.236.35.181:8080
curl -X POST -H 'Content-Type: application/json' -d '{"title": "test2", "description": "test2"}' $ONPREMISE_WEBSERVER_ENDPOINT/api/tutorials
```

# Database Migration

### Aurora target database creation
The database migration steps are inspired from this workshop:
https://catalog.us-east-1.prod.workshops.aws/workshops/77bdff4f-2d9e-4d68-99ba-248ea95b3aca/en-US/

- Deploy an Aurora MySQL 5.7 DB cluster with the following parameters:
```
DB admin user = dbadmin
DB admin password = 515e6deb49cb40c0
```
- Test connection to database:
```
mysql -u dbadmin -p --password=515e6deb49cb40c0 -D dbadmin -h aurora-mysql-instance-1.cmogbpmxhfsr.eu-central-1.rds.amazonaws.com
```


### Schema conversion
- Create a t3.xlarge Windows 2019 instance and install the AWS Schema Conversion Tool following these instructions:
https://docs.aws.amazon.com/SchemaConversionTool/latest/userguide/CHAP_Installing.html

- Follow the "New Project Wizard" to convert the DB Schema

- After the new Aurora MySQL schema is created, update the table definition:
```
alter table tutorials change ID ID INT NOTNULL AUTO_INCREMENT ;
drop trigger tutorials_bir ;
```


### Database Migration Service
Detailed instructions can be found here:
https://docs.aws.amazon.com/dms/latest/userguide/CHAP_Source.Oracle.html

- Grant Oracle account privileges (tried with dbadmin instead):
https://docs.aws.amazon.com/dms/latest/userguide/CHAP_Source.Oracle.html#CHAP_Source.Oracle.Self-Managed.Privileges


- Activate ARCHIVELOG:
https://orahow.com/how-to-enable-disable-archivelog-mode-in-oracle-11g-12c/

WARNING: execute the SQL command by starting sqlplus inside powershell directly on the VM.
```
sqlplus sys as sysdba
```

-  Activate logging:
```
SQL> ALTER DATABASE ADD SUPPLEMENTAL LOG DATA;
SQL> ALTER DATABASE ADD SUPPLEMENTAL LOG DATA (PRIMARY KEY) COLUMNS;
```

- Grant necessary rights to dbadmin:
```
sqlplus64 sys/ainu9Jui@//$DBSERVER_PRIVATE_IP/octank as sysdba
SQL> @grant_dms_priviledges.sol 
```

- Activate additional logging on tutorials table:
```
sqlplus64 dbadmin/515e6deb49cb40c0@//$DBSERVER_PRIVATE_IP:1521/octank
alter table tutorials add supplemental log data (ALL) columns;
```

# Webserver Migration

### Application Migration Service
- Follow the steps from this tutorial:
https://application-migration-with-aws.workshop.aws/en/app-mig-service.html

- To install the agent on the source webserver:
```
wget -O ./aws-replication-installer-init.py https://aws-application-migration-service-eu-west-3.s3.eu-west-3.amazonaws.com/latest/linux/aws-replication-installer-init.py
sudo python3 aws-replication-installer-init.py
```

- After starting the instance, update `src/main/resources/application.properties`:
```
spring.datasource.url=jdbc:mysql://aurora-mysql-instance-1.cmogbpmxhfsr.eu-central-1.rds.amazonaws.com:3306/dbadmin?useSSL=false&serverTimezone=UTC
```

### Application Modernization
- Create a container using this guide:
https://spring.io/blog/2018/11/08/spring-boot-in-a-container
```
# Check application is running well
./mvnw spring-boot:ru
# Create jar file
./mvnw clean package
# Check jar file is working
/usr/bin/java -jar /home/ubuntu/spring_demo/target/demo-0.0.1-SNAPSHOT.jar
# Transfer jar file to docker machine and create container:
docker build -t octank/webapp .
docker tag 9b5e505c7891 402542165744.dkr.ecr.eu-central-1.amazonaws.com/octank:latest
aws ecr get-login-password --region eu-central-1 | docker login --username AWS --password-stdin 402542165744.dkr.ecr.eu-central-1.amazonaws.com
```

- Create an `octank` ECR repository and upload the docker image following these instructions:
https://docs.aws.amazon.com/AmazonECR/latest/userguide/docker-push-ecr-image.html

- Create secrets using these instructions:
https://aws.amazon.com/premiumsupport/knowledge-center/ecs-data-security-container-task/
```
aws secretsmanager create-secret --name AURORA_DB_URL --secret-string 'jdbc:mysql://aurora-mysql-instance-1.cmogbpmxhfsr.eu-central-1.rds.amazonaws.com:3306/dbadmin?useSSL=false&serverTimezone=UTC'

# Use the following json file
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "ssm:GetParameters",
        "secretsmanager:GetSecretValue"
      ],
      "Resource": [
        "arn:aws:ssm:eu-central-1:402542165744:parameter/AURORA_DB_URL",
        "arn:aws:secretsmanager:eu-central-1:402542165744:secret:AURORA_DB_URL*"
      ]
    }
  ]
}
```



- Create an ECS service following the steps from this link:
https://application-migration-with-aws.workshop.aws/en/container-migration.html



# References
- Migration Lab:
https://application-migration-with-aws.workshop.aws/en/
- Migration Lab GitHub repository: 
https://github.com/aws-samples/application-migration-with-aws-workshop
- AWS Database Migration Workshop:
https://catalog.us-east-1.prod.workshops.aws/workshops/77bdff4f-2d9e-4d68-99ba-248ea95b3aca/en-US/oracle-aurora

https://oracle-base.com/articles/misc/autonumber-and-identity

https://medium.com/@mfofana/step-1-build-an-rest-application-with-spring-boot-and-oracle-run-in-docker-9a1a38a8af2c
