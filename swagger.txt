swagger: "2.0"
info:
  description: "This is a sample tutorials management API."
  version: "1.0.0"
  title: "Octank Spring Webapp POC"
  termsOfService: "http://swagger.io/terms/"
  contact:
    email: "apiteam@swagger.io"
  license:
    name: "Apache 2.0"
    url: "http://www.apache.org/licenses/LICENSE-2.0.html"
host: "webserver.octank.org"
basePath: "/api"
tags:
- name: "tutorials"
  description: "Manage tutorials"
  externalDocs:
    description: "Find out more"
    url: "http://swagger.io"
schemes:
- "https"
- "http"
paths:
  /tutorials:
    get:
      tags:
      - "tutorials"
      summary: "Get all tutorials"
      description: "Multiple status values can be provided with comma separated strings"
      operationId: "findPetsByStatus"
      produces:
      - "application/xml"
      - "application/json"
    
    post:
      tags:
      - "tutorials"
      summary: "Add a new tutorial"
      description: ""
      operationId: "addPet"
      consumes:
      - "application/json"
      - "application/xml"
      produces:
      - "application/xml"
      - "application/json"
      parameters:
      - in: "body"
        name: "body"
        description: "Pet object that needs to be added to the store"
        required: true
        schema:
          $ref: "#/definitions/Pet"
      responses:
        "405":
          description: "Invalid input"
      security:
      - petstore_auth:
        - "write:pets"
        - "read:pets"
    delete:
      tags:
      - "tutorials"
      summary: "Delete all tutorials"
      description: ""
      operationId: "deletePet"
      produces:
      - "application/xml"
      - "application/json"
      parameters:
      - name: "api_key"
        in: "header"
        required: false
        type: "string"
      - name: "petId"
        in: "path"
        description: "Pet id to delete"
        required: true
        type: "integer"
        format: "int64"
      responses:
        "400":
          description: "Invalid ID supplied"
        "404":
          description: "Pet not found"
      security:
      - petstore_auth:
        - "write:pets"
        - "read:pets"

  /tutorials/{Id}:
    get:
      tags:
      - "tutorials"
      summary: "Get tutorial by ID"
      description: "Returns a single pet"
      operationId: "getPetById"
      produces:
      - "application/xml"
      - "application/json"
      parameters:
      - name: "petId"
        in: "path"
        description: "ID of pet to return"
        required: true
        type: "integer"
        format: "int64"
      responses:
        "200":
          description: "successful operation"
          schema:
            $ref: "#/definitions/Pet"
        "400":
          description: "Invalid ID supplied"
        "404":
          description: "Pet not found"
      security:
      - api_key: []
    put:
      tags:
      - "tutorials"
      summary: "Update an existing tutorial"
      description: ""
      operationId: "updatePet"
      consumes:
      - "application/json"
      - "application/xml"
      produces:
      - "application/xml"
      - "application/json"
      parameters:
      - in: "body"
        name: "body"
        description: "Pet object that needs to be added to the store"
        required: true
        schema:
          $ref: "#/definitions/Pet"
      responses:
        "400":
          description: "Invalid ID supplied"
        "404":
          description: "Pet not found"
        "405":
          description: "Validation exception"
      security:
      - petstore_auth:
        - "write:pets"
        - "read:pets"

    delete:
      tags:
      - "tutorials"
      summary: "Deletes a tutorial"
      description: ""
      operationId: "deletePet"
      produces:
      - "application/xml"
      - "application/json"
      parameters:
      - name: "api_key"
        in: "header"
        required: false
        type: "string"
      - name: "petId"
        in: "path"
        description: "Pet id to delete"
        required: true
        type: "integer"
        format: "int64"
      responses:
        "400":
          description: "Invalid ID supplied"
        "404":
          description: "Pet not found"
      security:
      - petstore_auth:
        - "write:pets"
        - "read:pets"
  /tutorials/published:
    get:
      tags:
      - "tutorials"
      summary: "Get all published tutorials"
      description: "Multiple status values can be provided with comma separated strings"
      operationId: "findPetsByStatus"
      produces:
      - "application/xml"
      - "application/json"  

securityDefinitions:
  petstore_auth:
    type: "oauth2"
    authorizationUrl: "http://petstore.swagger.io/oauth/dialog"
    flow: "implicit"
    scopes:
      write:pets: "modify pets in your account"
      read:pets: "read your pets"
  api_key:
    type: "apiKey"
    name: "api_key"
    in: "header"
definitions:
  Tutorial:
    type: "object"
    required:
    - "name"
    - "photoUrls"
    properties:
      id:
        type: "integer"
        format: "int64"
        example: "12345"
      title:
        type: "string"
        example: "tutorial #123"
      description:
        type: "string"
        example: "ipsus lorem"
      published:
        type: "integer"
        example: "0/1"


externalDocs:
  description: "Find out more about Swagger"
  url: "http://swagger.io"
