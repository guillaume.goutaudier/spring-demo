CREATE OR REPLACE TRIGGER tutorials_bir 
BEFORE INSERT ON tutorials 
FOR EACH ROW 
WHEN (new.id IS NULL) 
BEGIN 
  :new.id := tutorials_seq.NEXTVAL; 
END;
/
