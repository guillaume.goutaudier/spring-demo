CREATE TABLE tutorials ( id NUMBER(10) NOT NULL, title VARCHAR2(255), description VARCHAR2(255), published NUMBER(1) DEFAULT 0, PRIMARY KEY(id) );
CREATE SEQUENCE tutorials_seq;
